package com.moisescruz.rest;

import com.moisescruz.rest.utils.BadSeparator;
import com.moisescruz.rest.utils.EstadosPedido;
import com.moisescruz.rest.utils.Utilidades;
import org.junit.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;


import static org.assertj.core.api.Fail.fail;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@EnableConfigurationProperties
@SpringBootTest //
public class EmpleadosControllerTest {
    //@Autowired
    //EmpleadosController empleadosController;

    @Test
    public void testgetCadena(){
        String correcto = "L.U.Z D.E.L S.O.L";
        String origen = "luz del sol";
        EmpleadosController empleadosController = new EmpleadosController();
        assertEquals(correcto,empleadosController.getCadena(origen,".")); //compara dos valores , si son iguales devuelve true
    }

    @Test
    public void testSeparadorGetCadena(){
        try{
            Utilidades.getCadena("MoisesCruz", "..");
            fail("se esperaba BadSeparator");
        } catch(BadSeparator bs){

        }

    }

    @Test
    public void testgetAutor(){
        EmpleadosController empleadosController = new EmpleadosController();
        assertEquals("MoisesC ruz",empleadosController.getAppAutor());
    }

    @ParameterizedTest // podemos indicar los valores a los uqe se sometera la prueba
    @ValueSource(ints = {1,3,5,16,-3, Integer.MAX_VALUE}) // se definen los valores
    public void testEsImpar(int nume){
        assertTrue(Utilidades.esImpar(nume));
    }

    @ParameterizedTest // podemos indicar los valores a los uqe se sometera la prueba
    @ValueSource(strings = {""," "}) // se definen los valores
    public void testEstaBlanco(String texto){
        assertTrue(Utilidades.estaBlanco(texto));
    }

    @ParameterizedTest // podemos indicar los valores a los uqe se sometera la prueba
    @NullAndEmptySource//ademas de lo del values , validara un null y una vacia por default
    @ValueSource(strings = {"  ","\t","\n"}) // se definen los valores
    public void testEstaBlancoCompleto(String texto){
        assertTrue(Utilidades.estaBlanco(texto));
    }

    @ParameterizedTest
    @EnumSource(EstadosPedido.class)
    public void testValorarEstadoPedido(EstadosPedido ep){
        assertTrue(Utilidades.valorarEstadosPedido(ep));

    }


}
