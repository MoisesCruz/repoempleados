package com.moisescruz.rest.repositorios;

import com.moisescruz.rest.empleados.Capacitacion;
import com.moisescruz.rest.empleados.Empleado;
import com.moisescruz.rest.empleados.Empleados;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.LoggerFactory;


@Repository
public class EmpleadoDAO {
    private static Empleados list = new Empleados();
    //todo se va a ejecutar antes de que se acceda a cualqueir informacion de esta clase
    //Logger logger = LoggerFactory.getLogger(EmpleadoDAO.class); //crea un log en la var logger
    Logger logger = LoggerFactory.getLogger(EmpleadoDAO.class);
    static {
        Capacitacion cap1 = new Capacitacion("2020/01/01", "DBA"); //crea objeto cap1 de tipo Capacitacion con los dos atributos inicializados
        Capacitacion cap2 = new Capacitacion("2019/01/01", "Back End");
        Capacitacion cap3 = new Capacitacion("2018/01/01", "Front End");
        ArrayList<Capacitacion> una = new ArrayList<Capacitacion>(); // crea la lista una
        una.add(cap1); // le arega la cap1
        ArrayList<Capacitacion> dos = new ArrayList<Capacitacion>();
        dos.add(cap1);
        dos.add(cap2);

        ArrayList<Capacitacion> todas = new ArrayList<Capacitacion>();
        todas.add(cap3);
        todas.addAll(dos);//agrega las caps de dos( que son la cap1 y la cap2)

        list.getListaEmpleados().add(new Empleado(1,"Antonio","lopez","antonio@lopez.com",una));
        list.getListaEmpleados().add(new Empleado(2,"Jorge","Sanchez","antonio2@lopez.com",dos));
        list.getListaEmpleados().add(new Empleado(3,"Juan","Cardona","antonio3@lopez.com",todas));
    }
    public Empleados getAllEmpleados(){
        logger.debug("Empleados devueltos");
        return list;
    }
    public Empleado getEmpleado(int id){
        for (Empleado emp: list.getListaEmpleados()){
            if (emp.getIds() == id) {
                return emp;
            }
        }
        return null;
    }
    public void addEmpleado(Empleado emp){
        list.getListaEmpleados().add(emp);
    }

    public void updEmpleado(Empleado emp){
        Empleado current = getEmpleado(emp.getIds());
        // se debe de caputar para generar el 404
        current.setNombre(emp.getNombre());
        current.setApellido(emp.getApellido());
        current.setEmail(emp.getEmail());
    }
    public void updEmpleado(int id,Empleado emp){
        Empleado current = getEmpleado(id);
        // se debe de caputar para generar el 404
        current.setNombre(emp.getNombre());
        current.setApellido(emp.getApellido());
        current.setEmail(emp.getEmail());
    }
    public String deleteEmpleado(int id){
        Empleado current = getEmpleado(id);
        if(current == null){
            return "no";
        }
        Iterator it = list.getListaEmpleados().iterator(); // iterador de getListaEmpleados simula el for para buscar los elementos
        while (it.hasNext()){ // mientras haya elementos realizara el while
            Empleado emp =(Empleado) it.next(); // el objeto siguiente lo guarda en emp
            if (emp.getIds() == id){ // cuando lo encuentre
                it.remove(); // lo borra de it
                break;
            }
        }
        return "ok";
    }

    public void softUpdEmpleado(int id, Map<String,Object> updates){ //lista de elementos y cada elemento tendra string y objet en el primero vendra el nombre del campo a modificar y en el object el valor que preevalecera
        Empleado current = getEmpleado(id); // id que llego en la invocacion
        //se debe de agregar fuuncion para cuando no encuentra el id
        for(Map.Entry<String,Object> update: updates.entrySet()){ // devuelve los elementos del mapa en forma de array de entrys
            switch (update.getKey()){ // obtiene la llave del Map
                case "nombre":
                    current.setNombre(update.getValue().toString());
                    break;
                case"apellido":
                    current.setApellido(update.getValue().toString());
                    break;
                case "email":
                    current.setEmail(update.getValue().toString());
                    break;

            }
        }
    }
    public List<Capacitacion> getCapacitacionEmpleado(int id){//id para buscar las caps de ese id
      Empleado current = getEmpleado(id); //se pasa el valor a current para buscarlo con el metodo getEmpleado
      ArrayList<Capacitacion> caps = new ArrayList<Capacitacion>(); // levanta la instancia caps vacia
      if (current != null) caps = current.getCapacitaciones(); //si no es vacio va a buscar las capacitaciones con getcapacitacionmes y la asigna a current
      return caps;
      //implementar respuestas not found
    }

    public Boolean addCapacitacion(int id,Capacitacion cap){
        Empleado current = getEmpleado(id);
        if(current == null){
            return false;
        }
        current.getCapacitaciones().add(cap);//recupera la lista de capacitaciones y agrega la que entro al invocar al metodo
        return true;
    }
}
