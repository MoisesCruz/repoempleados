package com.moisescruz.rest;


import com.moisescruz.rest.empleados.Capacitacion;
import com.moisescruz.rest.empleados.Empleado;
import com.moisescruz.rest.empleados.Empleados;
import com.moisescruz.rest.repositorios.EmpleadoDAO;
import com.moisescruz.rest.utils.Configuracion;
import com.moisescruz.rest.utils.EstadosPedido;
import com.moisescruz.rest.utils.Utilidades;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.websocket.server.PathParam;
import java.net.URI;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(path="/empleados")
public class EmpleadosController {
    @Autowired
    private EmpleadoDAO empDao;

    @GetMapping(path = "/")
    public Empleados getEmpleados(){
    return empDao.getAllEmpleados();
    }

    @GetMapping (path = "/{id}")
    public ResponseEntity<Empleado> getEmpleado(@PathVariable int id){
        Empleado emp = empDao.getEmpleado(id);
        if (emp == null){
            return ResponseEntity.notFound().build();
        }else
        return ResponseEntity.ok().body(emp); //
    }

    @PostMapping("/")
    public ResponseEntity<Object> addEmpleado(@RequestBody Empleado emp){ //el dato emp de tipo Empleado debe de enviarse en el body para poderla capturar con el requestbody
        Integer id = empDao.getAllEmpleados().getListaEmpleados().size() + 1;
        emp.setIds(id);
        empDao.addEmpleado(emp);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(emp.getIds())
                .toUri();
        return ResponseEntity.created(location).build(); // la respuesta la pone en el header con respuesta 201
    }

    @PutMapping(path = "/",consumes = "application/json",produces = "aplication/json")
    public ResponseEntity<Object> updEmpleado(@RequestBody Empleado emp){
        empDao.updEmpleado(emp);
        return ResponseEntity.ok().build();
    }

    @PutMapping(path = "/{id}",consumes = "application/json",produces = "aplication/json")
    public ResponseEntity<Object> updEmpleado(@PathVariable int id, @RequestBody Empleado emp){
        empDao.updEmpleado(id, emp);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Object> delEmpleadoId(@PathVariable int id){
        String resp = empDao.deleteEmpleado(id);
        if(resp == "ok"){
            return ResponseEntity.ok().build();
        } else{
            return ResponseEntity.notFound().build();
        }
    }

    @PatchMapping(path = "/{id}",consumes = "application/json",produces = "aplication/json")
    public ResponseEntity<Object> softUpdEmpleado(@PathVariable int id, @RequestBody Map<String,Object> updates){
        empDao.softUpdEmpleado(id,updates);
        return ResponseEntity.ok().build();
    }

    @GetMapping(path = "/{id}/capacitaciones")
    public ResponseEntity<List<Capacitacion>> getCapacitacionesEmpleado(@PathVariable int id){
        return ResponseEntity.ok().body(empDao.getCapacitacionEmpleado(id));//con el response el indicas el tipo en este caso lsita de capaciacion acorde a la linea de arriba con la funcion ok es un 200,
    }

    @PostMapping(path = "/{id}/capacitaciones",consumes = "application/json",produces = "aplication/json")
    public ResponseEntity<Object> addCapacitacionEmpleado(@PathVariable int id,@RequestBody Capacitacion cap){
        if (empDao.addCapacitacion(id,cap)){
            return ResponseEntity.ok().build();

        }else{
            return ResponseEntity.notFound().build();
        }
    }

    //Accesos a propiedades
    @Value("${app.titulo}") private String titulo;
    @GetMapping("/titulo")
    public String getAppTitulo(){
        String modo = configuracion.getModo();
        return String.format("%s %s", titulo , modo); //concatena con el %s y va a poner lo que hay en las variables titulo y modo
    }

    @Autowired
    private Environment env;//la clase da aacceso a todo lo qeu haya en las propiedades
    @GetMapping("/autor")
    public String getAppAutor(){
        //return env.getProperty("app.autor");
        return configuracion.getAutor();

    }
    @Autowired
    Configuracion configuracion;

    @GetMapping("/cadena")
    public String getCadena(@RequestParam String texto,@RequestParam String separador){

        try {

            return Utilidades.getCadena(texto, separador);
        } catch (Exception e){
            return "";
        }
    }

}

