package com.moisescruz.rest.utils;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component//necesario para identificar  a la calse para que pueda accerder a ls propiedades
@ConfigurationProperties("app")// indica que tipo de propiedades se van a poder acceder con esta clase en este caso solo las de app

public class Configuracion {
    private String titulo;
    private String autor;
    private String modo;

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getModo() {
        return modo;
    }

    public void setModo(String modo) {
        this.modo = modo;
    }
}
