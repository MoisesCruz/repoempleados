package com.moisescruz.rest.utils;

public class Utilidades {

    public static String getCadena(String texto, String separador) throws BadSeparator {
        if (separador.length() > 1 || (separador.equals(" "))) throw new BadSeparator();
        char[] letras = texto.toUpperCase().toCharArray();
        String resultado = "";
        for (char letra : letras) {
            if (letra != ' ') {
                resultado += letra + separador;
            } else {
                if (!resultado.trim().equals("")) {
                    resultado = resultado.substring(0, resultado.length() -1);
                }
                resultado += letra;
            }
        }
        if (resultado.endsWith(separador)) {
            resultado = resultado.substring(0, resultado.length() -1);
        }
        return resultado;
    }

    public static boolean esImpar(int num){
        return (num %2 !=0);
    }

    public static boolean estaBlanco(String texto){
        return texto == null || texto.trim().isEmpty();
    }

    public static boolean valorarEstadosPedido(EstadosPedido ep){
        int valor = ep.ordinal();// relaciona del 0-n acorde a la definicion de los valores en la enumeracionclase
        return (valor >=0 && valor <=4);
    }

}
