package com.moisescruz.rest.utils;

public class BadSeparator extends Exception{
    @Override
    public String getMessage() {
        return "separador incorrecto";
    }
}
