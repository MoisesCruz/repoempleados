package com.moisescruz.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestEmpleadosApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestEmpleadosApplication.class, args);
	}

}
