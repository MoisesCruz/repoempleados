package com.moisescruz.rest.empleados;

import java.security.PrivateKey;
import java.util.ArrayList;

public class Empleado {
    private Integer ids;
    private String nombre;
    private String apellido;
    private String email;
    private ArrayList<Capacitacion> capacitaciones;

    public Empleado(Integer ids, String nombre, String apellido, String email,ArrayList<Capacitacion> pcapacitaciones) {
        this.ids = ids;
        this.nombre = nombre;
        this.apellido = apellido;
        this.email = email;
        this.capacitaciones = pcapacitaciones;
    }

    public Integer getIds() {
        return ids;
    }

    public void setIds(Integer ids) {
        this.ids = ids;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Empleado() {
    }

    public ArrayList<Capacitacion> getCapacitaciones() {
        return capacitaciones;
    }

    public void setCapacitaciones(ArrayList<Capacitacion> capacitaciones) {
        this.capacitaciones = capacitaciones;
    }

    @Override
    public String toString() {
        return "Empleado{" +
                "ids=" + ids +
                ", nombre='" + nombre + '\'' +
                ", apellido='" + apellido + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
