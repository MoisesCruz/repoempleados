package com.moisescruz.rest.empleados;

import java.util.ArrayList;
import java.util.List;

public class Empleados {
    private List<Empleado> listaEmpleados;
    public  List<Empleado> getListaEmpleados(){ //List es una interfaz generica
        if (listaEmpleados == null){
          listaEmpleados = new ArrayList<>();  //genera un arreglo vacio en listEmpleados
        }
        return listaEmpleados;
    }

    public void setListaEmpleados(List<Empleado> listaEmpleados) {
        this.listaEmpleados = listaEmpleados;
    }
}
